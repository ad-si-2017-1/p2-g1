// configuracao do express
var express = require('express');
var app = express();
//var app = require('express')();  // módulo express
var server = require('http').Server(app);
var cookieParser = require('cookie-parser');  // processa cookies
app.use(cookieParser());
var bodyParser = require('body-parser');  // processa corpo de requests
app.use(bodyParser.urlencoded( { extended: true } ));
// configuracao do socket.io
var socketio = require('socket.io')(server);
var socketio_cookieParser = require('socket.io-cookie');
socketio.use(socketio_cookieParser);
server.listen(3000, function() {
    console.log('Listening at: http://localhost:3000');
});
socketio.listen(server);
var irc = require('irc');
var fs = require('fs');
var path = require('path');	// módulo usado para lidar com caminhos
app.use(express.static('./public'));
var participantes = {}; // mapa de participantes
var user_id = 1;

var Participante = function(servidor, nick, canal) {
    this.id = user_id++;
    this.servidor = servidor;
    this.nick = nick;
    this.canal = canal;
    this.socket = null;
    this.url = null;
    this.irc_client = new irc.Client(
        servidor,
        nick,
        {channels: [canal],});
    participantes[this.id] = this;

    socketio.on('connection', function (socket) {
        // o user_id pode ser obtido a partir do websocket
        var user_id = socket.request.headers.cookie.id;
        // executa somente se o socket ainda não foi criado
        if ( !participantes[user_id].socket ) {
            participantes[user_id].socket = socket;
            socket.on('message', function (msg) {
                if (msg.substring(0,1) == '/') {
                    var user_id = this.request.headers.cookie.id;
                    participantes[user_id].processar_comando(msg);
                }
                else {
                    var user_id = this.request.headers.cookie.id;
                     console.log('Message Received: '+msg+' de user_id: '+user_id);
                    var client = participantes[user_id].irc_client;
                    client.say(client.opt.channels[0], msg);
                }
            });
        }
    });
    this.processar_comando = function(msg) {
        switch(msg){
            case "/list":
            var canal = this.irc_client.opt.channels[0];
            this.irc_client.send('names', canal);
            break;
            case "/motd":
                this.irc_client.send('motd');
                break;
            case "/part":
                var canal = this.irc_client.opt.channels[0];
                var nick = this.opt.nick;
                this.irc_client.send('part', nick, canal, reason, msg);
                break;
            case "/quit":
                var canal = this.irc_client.opt.channels[0];
                this.irc_client.send('quit', canal);
                break;
            case "/kill":
                var canal = this.irc_client.opt.channels[0];
                this.irc_client.send('kill', canal);
                break;
            case "/notice":
                var canal = this.irc_client.opt.channels[0];
                var nick = this.opt.nick;
                this.irc_client.send('notice', nick, to, text, msg);
                break;
            case "/ping":
                var canal = this.irc_client.opt.channels[0];
                this.irc_client.send('ping', canal);
                break;
            case "/pm":
                var canal = this.irc_client.opt.channels[0];
                this.irc_client.send('pm', canal);
                break;
            case "/join":
                var canal = this.irc_client.opt.channels[0];
                var nick = this.opt.nick;
                this.irc_client.send('pm', canal,nick,msg);
                break;





        }
    };
    // inclui a propriedade user_id no irc_client
    this.irc_client.user_id = this.id;

    this.irc_client.addListener('message'+canal, function (from, message) 	 {
        var socket = participantes[this.user_id].socket;
        socket.emit('message', {"timestamp":Date.now(),
            "nick":from,
            "msg":message} );
    });
    this.irc_client.addListener('names', function(channel, nicks) {
        var lista = channel+': '+JSON.stringify(nicks);
        console.log('listagem de nicks do canal:'+lista);
        var socket = participantes[this.user_id].socket;
        socket.emit('message', {"timestamp":Date.now(),
            "nick":'status',
            "msg":"listagem de nicks do canal "+lista});
    });


    this.irc_client.addListener('motd', function(message) {
        console.log('motd: ', message);
    });
    this.irc_client.addListener('names', function(message) {
        console.log('lista nomes: ', message);
    });
    this.irc_client.addListener('topic', function(message) {
        console.log('Topicos: ', message);
    });
    this.irc_client.addListener('part', function(message) {
        console.log('Part: ', message);
    });
    this.irc_client.addListener('quit', function(message) {
        console.log('Quit: ', message);
        var user_id = this.request.headers.cookie.id;
        var client = participantes[user_id].irc_client;
        client.disconnect();
    });
    this.irc_client.addListener('kick', function(message) {
        console.log('Kick: ', message);
    });
    this.irc_client.addListener('kill', function(message) {
        console.log('Kill: ', message);
    });
    this.irc_client.addListener('notice', function(nick,to,text,msg) {
        console.log('Notice: ', message);
    });
    this.irc_client.addListener('ping', function(message) {
        console.log('ping: ', message);
    });
    this.irc_client.addListener('pm', function(message) {
        console.log('Pm: ', message);
    });

    this.irc_client.addListener('error', function(message) {
        console.log('error: ', message);
    });

    this.irc_client.addListener('registered', function(message) {
        this.irc_client.send('motd', canal);
        console.log('conectado:'+JSON.stringify(message));
    });


    this.irc_client.addListener('join', function(message) {
        if ( ! this.url ) {
            var socket = participantes[this.user_id].socket;
            this.url = 'irc://'+this.opt.nick+'@'+
                this.opt.server+'/'+
                this.opt.channels[0];
            socket.emit('message', {"timestamp":Date.now(),
                "nick":'status',
                "msg":"conectado"+this.url});
        }
    });
}


app.get('/', function (req, res) {
    if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
        var usuario =	new Participante(
            req.cookies.servidor,
            req.cookies.nick,
            req.cookies.canal);
        res.cookie('id', usuario.id);
        res.sendFile(path.join(__dirname, '/index.html'));
    }
    else {
        res.sendFile(path.join(__dirname, '/login.html'));
    }
});

app.post('/login', function (req, res) {
    res.cookie('nick', req.body.nome);
    res.cookie('canal', req.body.canal);
    res.cookie('servidor', req.body.servidor);
    res.redirect('/');
});


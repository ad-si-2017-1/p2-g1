# PROJETO 2 GRUPO 1

# INTEGRANTES

- Erick Vinicius (DOC)
- Anderson Martins (DEV)
- Silvio Passos (DEV)


# OBJETIVOS


- O projeto 2 tem como objetivo ** Utilizar o protocolo IRC, de nele desenvolver uma interface WEB utilizando WebSocket (Socket.io)**
 [Link do projeto que servirá de base para construção](https://gitlab.com/marceloakira/p2/)

Este protótipo está incompleto em funcionalidades. Ainda faltam vários pontos, tais como: suporte a vários canais, suporte a comandos, lista de participantes no canal, mensagens privadas,  permissões em canal ('modes'), etc. Cada grupo vai tentar desenvolver o máximo de funcionalidades possível, visando atingir níveis altos em aspectos teóricos e técnicos da disciplina.

# Aspectos teóricos necessários para o desenvolvimento
    O projeto envolve as seguintes teorias:
- Web Services: SOA, REST, JSON;
- Comunicação assíncrona: eventos, notificações, funções de callback;
- Comunicação solicitação/resposta: pooling, cookies;
- Programação web: roteamento, métodos GET/POST;
- Arquitetura proxy;
- Comunicação socket: protocolo IRC;
- Arquitetura publish/subscribe

# Aspectos técnicos necessários para o desenvolvimento

- Frontend: JQuery, cookies, HTML/Forms, DOM, etc;
- Backend: Nodejs, IRC, express;
- WebSockets: Socket.io

# REFERENCIAS

- [Markdown Sintaxe - .MD](https://sourceforge.net/p/modeloslibreoffice/tickets/markdown_syntax) 
- [Npm Javascript Package Manager](https://github.com/npm/npm)
- [Documentação API IRC library](https://node-irc.readthedocs.io/en/latest/API.html#client)
- [Documentação SocketIO](https://socket.io/docs/)
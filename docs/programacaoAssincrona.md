# Programação Assíncrona

JavaScript é assíncrono por natureza e por isso o Node também é.
A programação assíncrona é um padrão de projeto que assegura a execução de código não-bloqueante.
Código não-bloqueante não impede a execução de uma parte do código.
Geralmente, se o código for executado de forma síncrona, ou seja, um após o outro, não será necessário parar a execução desses códigos que não depende do que você está executando.
Assincronismo faz exatamente o oposto, código assíncrono executa sem ter qualquer dependência e sem ordem. Isso melhora a eficiência do sistema.
A programação assíncrona é excelente para uma execução mais rápida de programas, mas isso tem seu preço. Isso mesmo, é difícil de programar e na maioria das vezes acabamos tendo um cenário de inferência de retorno .
A proposta deste tutorial é explicar cada um dos cenários assíncronos que você pode enfrentar ao codificar. Nós também aprenderemos sobre como evitar eficientemente a situação do CallBack Hell.

###Começando

Vamos considerar o seguinte código:
```javascript
var fs = require("fs");
var fileContent = fs.readFileSync('sync.js','utf8');
console.log(fileContent);
console.log("something else");

```
No código acima, o arquivo foi lido primeiro e depois **console.log()** é executado.


Agora considere o seguinte código.
```javascript
var fs = require("fs");
fs.readFile('async.js','utf8',function(err,data){
    if(!err) {
       console.log(data);
    }
});
console.log("something else");
```
Aqui a saída será a seguinte:

```
something else
var fs = require("fs");
fs.readFile('sync.js','utf8',function(err,data){
    if(!err) {
       console.log(data);
    }
});
console.log("something else");
```
Aqui temos o conteúdo do **console.log()** primeiro e depois o conteúdo do arquivo.
Isso ocorre porque o código é assíncrono e o loop de eventos é executado posteriormente.

###O que é o Callback Hell?

Familiar com algo como este.
```javascript
var fs = require("fs");
var db = require('somedbfile.js');
var sendEmail = require('someEmail.js');
fs.readFile('async.js','utf8',function(err,data){
    if(!err) {
       console.log(data);
    }
    db.executeQuery('SELECT * FROM test',function(err,rows) {
      if(!err) {
        console.log("Error",err);
      }
      sendEmail(rows,function(err,data) {
        if(!err) {
          console.log("Error",err);
        }
        console.log("Operation done, i am in callback hell");
      });
    });
});
```
Nota: O código acima é apenas um exemplo, não um código de trabalho.

Isso acontece devido à natureza assíncrona do JavaScript.
Queremos executar tarefas que são dependentes umas das outras, assim,
as envolvemos nos retornos de retorno de cada função e, portanto, desta forma temos a situação de **Callback Hell**.


Algumas formas de evitar o **Callback Hell** ou (inferno de retorno).

Para evitá-lo, podemos seguir pelo menos uma das seguintes opções:

* Modularizar o código;
* Utilizar generators;
* Utilizar promise;
* Utilizar programação orientada a eventos;
* Utilizar Async.js

###Modularize seu código

Considere o seguinte código:

```javascript
var fs = require("fs");
fs.readFile('async.js','utf8',function(err,data){
    if(!err) {
       console.log(data);
    }
});
```
A função de retorno de chamada é um encerramento e só pode ser acessado dentro da função.
No entanto, você pode criar uma função separada fornecendo algum nome e passar essa função como callback.
Como isso.
```javascript
var fs = require("fs");

fs.readFile('async.js','utf8',fileContent);

function fileContent(err,data) {
  if(!err) {
     console.log(data);
  }
}
```
A única desvantagem é que você irá precisar criar diversas funções quando o código cresce.

###O uso de generators

De forma simples, o uso de **Generators** (geradores) fornece a capacidade de converter código assíncrono para um síncrono.
Permite aprender usando o exemplo, considere seguir.
```javascript
var fs = require("fs");
fs.readFile('async.js','utf8',function(err,data){
    if(!err) {
       console.log(data);
    }
});
console.log("something else");
```
Este é um código assíncrono para que **console.log()** seja executado antes de **readFile()**.
Para evitar colocar o **console.log()** dentro do fechamento de retorno, podemos usar geradores para converter a natureza assíncrona de **readFile()** em um síncrono.
Considere o seguinte código:
```javascript
"use strict";
var fs = require('fs');
var co = require('co');

co(function* () {
  var file1 = yield readFile('demo.js');
  console.log(file1);
  console.log("I am after file read even though its Async");
});

function readFile(filename) {
  return function(callback) {
    fs.readFile(filename, 'utf8', callback);
  };
}
```
Tente executar este código, você verá o conteúdo do arquivo primeiro e, em seguida, **console.log()**.


###Utilizar o Promise

**Promise** representa o resultado da função assíncrona. **Promise** podem ser usadas para evitar encadeamento de callbacks. Em Node.js, você pode usar os  módulos **Q** ou **Bluebird** para aproveitar o recurso do **Promise**.
Se quisermos converter arquivo de retorno de código de leitura em **Promise** veremos como podemos fazer isso.
Antes de prosseguir, certifique -se de ter bluebird **(nmp install bluebird)** instalado.
```javascript
var Promise = require('bluebird');
 // Converts all function of 'fs' into promises.
var fs = Promise.promisifyAll(require('fs'));

fs.readFileAsync('file.js','utf8')
// 'then' when result comes.
.then(function(data) {
  console.log(data);
})
//'catch' when error comes.
.catch(function(err) {
  console.log(err);
});
```
Podemos verificar a documentação da [API bluebird](http://bluebirdjs.com/docs/api-reference.html), para maiores informações.

###Utilizar a programação orientada a eventos

**Node.js** nos fornece o módulo _EventEmitter_ que pode nos ajudar a programar usando eventos.
Também podemos usá-lo para estruturar seu código e evitar o **Callback Hell**(inferno de retorno). 
No entanto, talvez não ajude, caso o nosso código possua sua estrutura grande, mas é uma opção.

Considere o código abaixo:
```javascript
var EventEmitter = require('events').EventEmitter;
var emitter = new EventEmitter();
var fs = require('fs');
// Event to read file - generic function.
emitter.on('start_read',function(file_name) {
  console.log("Started Reading file....\n\n");
  fs.readFile(file_name, 'utf8', function (err,data) {
    if (err) {
      console.log("Error happens.");
    } else {
      console.log("File data", data);
    }
  });
});
// Let's read some file.
emitter.emit('start_read','env.json');
emitter.emit('start_read','envConfig.js');
```
Aqui criamos uma função genérica que executa o arquivo lido.
Podemos chamar esta função emitindo eventos.
Também podemos estendê-lo para emitir os eventos quando a leitura de arquivos é realizada.

###Utilizar Async.js

**Async.js** é um módulo utilitário que fornece várias funções (aproximadamente 70) para lidar com o JavaScript assíncrono.
Esta é a maneira mais preferida e maneira recomendada por especialistas.
Vamos olhar uma situação comum que você pode ter enquanto escreve um código e uma solução proposta para o mesmo usando **Async.js**.

**Cenário 1: Execute várias tarefas que não dependem umas das outras e quando todas terminarem, fazer outra coisa**

Neste cenário, queremos executar várias funções assíncronas que não dependem umas das outras, ou seja, não precisamos bloqueá-las.

**Solução**: Use a função **async.parallel()**.<br>
Aqui está o código para explicar o mesmo:
```javascript
var async = require('async');

async.parallel([
    function(callback) {
      // Some Async task
      callback();
    },
    function(callback) {
      // Some Async task
      callback();
    }
  ],function(err,data) {
  // Code to execute when everything is done.
});
```
**Cenário 2: Executar várias tarefas uma após a outra e trocar dados entre elas e uma vez que eles são terminar executar outra coisa.**
Este é o cenário muito semelhante ao anterior, exceto que precisamos passar alguns dados para a próxima função. **Async.series()** passará cada funções de dados para a função de retorno final não para a próxima.

**Solução**: Use a função **async.waterfall().**

Aqui está o código para explicar o mesmo:
```javascript
var async = require('async');
async.waterfall([
    function(callback) {
      // some code to execute
      // in case to go to next function provide callback like this.
      callback(null,valueForNextFunction);
      // Got some error ? Don't wanna go further.
      // Provide true in callback and execution will stop.
      //callback(true,"Some error");
    },
    function(parameterValue,callback) {
      // Some code to execute.
      callback(null,"Some data");
    }
  ],function(err,data) {
  // Code to execute after everything is done.
});
```
**Cenário 3: Executar tarefa paralela múltipla, iterando sobre toda a coleção, e quando todos eles terminem, executam algo mais.**
Considere um cenário onde você precisa enviar um milhão de e-mails, onde serão enviados 1000 e-mails por vez.
Será necessário estabelecer um limite para divisão dos dados a enviar.

**Solução**: Utilizar **async.each().**

Aqui está o código para explicar o mesmo:
```javascript
var async = require('async');
var emails = ["abc@xyz.com","blahblah@aa.com"];

async.eachLimit(emails,1000,function(singleEmail,callback) {
  // Emailer code
  // singleEmail will be one value at a time.
},function(err,data) {
  // Once all done, comes here.
});
```
**Cenário 4: Execute várias tarefas paralelas iterando sobre uma matriz (qualquer coleção) e dentro de cada tarefa paralela executar algumas tarefas em série e quando todos eles concluirem, executar outra coisa**

Considere o mesmo cenário de e-mail, depois de enviar um e-mail, você também precisa atualizar o banco de dados.
Isso significa que você precisa executar duas tarefas para cada e-mails e que também em série, passando dados da função **email()** para o banco de dados.

Podemos utilizar **async.each()** combinada com **async.waterfall()**.

Aqui está o código para explicar o mesmo:
```javascript
var async = require('async');
var emails = ["abc@xyz.com","blahblah@aa.com"];
async.each(emails,function(singleEmail,callback) {
  async.waterfall([
    function(callback) {
      // code to send email.
      callback(null,Flag);
    },
    function(emailSentOrNot,callback) {
      // Update DB.
    }
  ],function(err,data) {
  });
},function(err,data) {
  // Once all done, comes here.
});
```
Basicamente, podemos usar qualquer combinação de funções **async.js** para cumprir essa exigência.<br>
Aqui está um exemplo de combinação:

```javascript
var async = require('async');
async.forEach(someData,function(singleData,callback){
  async.series();
  //OR
  async.paralle();
  //OR
  async.waterfall();
},function(err,data) {
  // final callback
});
// going more deep.
async.forEach(someData,function(singleData,callback){
  async.waterfall([
      function(callback) {
        async.forEachLimit(somedata,100,function(singleData,callback){
          // You can use more combo here too.
          callback(null);
        },function(err) {
          // final callback
          // Now call the callback of waterfall.
          callback(null,"No error");
        });
      }
    ],function(err,data) {
      // callback of top async.forEach()
      callback(null);
  });
},function(err) {
  // final callback
});
```
**Executar operação de fila**

Considerando e-mails em massa em um cenário do mundo real,
você não pode invocar como milhões de função de retorno de chamada em um determinado momento e é por causa da limitação de recursos e buffer.
Neste tipo de situação onde a operação deve invocar em lote dizer 10000 e-mails ao mesmo tempo e manter a execução até que tudo seja feito você pode usar **async.queue()**.

Aqui está um exemplo de fila muito simples para enviar um e-mail.
```javascript
var async = require('async');
// Send email
var sendEmail = function(email,callback) {
  console.log("Sending email to "+email);
  callback(null);
}
// create a queue object with concurrency 2
var q = async.queue(sendEmail,2);
// Called when every processing is done
q.drain = function() {
    console.log('all emails sent');
}
// add some emails to the queue
q.push(["rwtc66@gmail.com","shahid@codeforgeek.com"]);
// add email to the front of the queue
q.unshift("abc@gmail.com");
//output
/*
Sending email to abc@gmail.com
Sending email to rwtc66@gmail.com
Sending email to shahid@codeforgeek.com
all emails sent
*/
```
###Conclusão
Cobrimos algumas funções importantes e úteis como a função **async.js**, no entanto, 
existem muitos cenários no âmbito da programação em Node.js, e você pode utilizar estas funções de acordo com seu cenário de programação.
**Async.js** é sem dúvida pacote muito útil para desenvolvedor Node.js.
Ele vai economizar muito tempo e melhorar o seu código também.
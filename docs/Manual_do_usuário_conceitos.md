# Introdução

## Histórico

O IRC (Internet Relay Chat) é um protocolo utilizado na Internet como troca de arquivos e de informações. Desenvolvido em 1988 pelo programador Jarkko Oikarinen, só foi utilizado de maneira formal em 1993. O objetivo da criação foi desenvolver um sistema compatível a TCP/IP e SSL, com capacidade e armazenamento de conversas entre muitos usuários simultaneamente. A primeira rede com IRC surgiu em Universidades da Finlândia e em 1993, o sistema foi utilizado para informar as notícias em tempo real pela internet.

O modo de comunicação e canais do IRC é a conversação de um canal, no qual os usuários enviam mensagens ao servidor que as reenvia a todos do mesmo canal. Como o IRC é um protocolo de texto, pode ser utilizado através de um servidor como o netcat ou telnet. No início do ano de 2000, o IRC tornou-se o principal sistema da internet com capacidade para suportar o bate-papo e concentrar milhões de usuários ao mesmo tempo. O gerenciamento de dados e a facilidade de interação eram insuperáveis. O sistema era monitorado pelos Operadores (OPs), os quais gerenciavam o canal para o bom funcionamento do mesmo, e expulsavam os que queriam confusão.

Em meados de 2003 com o lançamento dos softwares de mensagem instantânea, cuja tecnologia permitia mais recursos que o sistema IRC, o protocolo se tornou menos utilizado. O mais famoso é o MSN Messenger, que permite conversa de voz e vídeo, além de links a portal de jogos e também salas de chat. Mais tarde, com o surgimento das redes sociais, o Orkut acabou com a rede de IRC, pois nele o usuário participa de comunidades com sistemas de chat e interface com MSN.

O IRC pode ser utilizado para execução de Botnets, onde o usuário lança o chamado Spam a milhares de computadores. Atualmente o IRC é utilizado em algumas redes ativas. E por pessoas que se interessam por estudar o desenvolvimento do sistema de protocolo de redes, como canais de troca de arquivos e suporte a usuários Linux. Atualmente as redes IRC estão por todo o mundo, dentre as maiores, podemos citar, AustNet, DALnet, PTnet, MindForge e Rizon. A maior rede IRC do mundo em quantidade de usuários é a Quakenet.

## Comandos IRC

Os comandos do IRC devem ser acompanhados da barra para a direita (/), indicando o comando correspondente. Como segue: 

* /msg (nome) (mensagem) Manda uma mensagem no PVT, mesmo se o nick não tiver no mesmo canal que você. 
* /join (canal) Entra em um canal. 
* /list (nome) Lista todos os canais disponiveis no servidor. 
* /quit (servidor), que fecha a conexão IRC.
* /nick (nome) Muda de Nome.

## Componentes

Os principais componentes do Internet Relay Chat são:

* Clientes
* Servidores
* Operadores
* Canais
* Operadores de Canais

## Clientes

Um cliente é qualquer conexão a um servidor, que não seja um servidor. Eles são diferenciados através de um apelido (ou nickname) único com tamanho máximo de nove caracteres. Além disso, todos os servidores devem ter as seguintes informações sobre os clientes: o host do cliente, o seu username e o nome do servidor ao qual este está conectado.

## Servidores

Os Servidores formam o backbone do IRC e são os pontos de conexão para outros servidores e pra clientes. A configuração de uma rede IRC assemelha-se a uma spanning tree, onde cada servidor age como um nodo central para o resto da rede que ele está conectado.

## Operadores

Os operadores são uma classe especial de clientes capazes de realizarem funções gerais de manutenção na rede IRC. A principal função dos operadores é desconectar e reconectar servidores, para previnir o mau uso de roteamento. Além desta, os operadores podem realizar outras operações, como por exemplo, desconectar usuários.

##  Canais

Um canal é um grupo formado por um ou mais clientes que irá receber mensagens endereçadas a este canal. O canal é criado implicitamente quando o primeiro cliente junta-se a este canal (comando join). De forma análoga o canal deixa de existir quando o último cliente o deixa (comando part).

## Operadores de Canais

Operadores de canais são clientes que são considerados donos de determinados canais. Quando um novo canal é criado, o criador é o operador deste canal. Um operador de canal pode remover clientes deste, mudar o modo do canal, tornar outros clientes operadores deste canal, mudar o tópico do canal, etc. O operador de canal é identificado através do símbolo '@' que precedo o nickname do cliente.

Para entender mais sobre o protocolo IRC acesse https://gitlab.com/ad-si-2017-1/p1-g1/blob/master/Documentacao/Documentacao.md 

# Especificação Web IRC

## Sobre

Quando um usuário se conecta através de um método indireto, tais clientes da Web, o cliente indireto envia seu próprio endereço IP em vez de enviar o endereço IP do usuário, a menos que o WebIRC seja implementado pelo cliente e pelo servidor.

## Método

Antes que o cliente envie os comandos PASS, USER ou NICK deve enviar:

  WEBIRC senha nome de host de usuário ip
  
Quando cada um dos tokens acima referidos for substituído pelo seguinte:

* password: Senha que autentica o comando WEBIRC deste cliente.
* user: Usuário ou cliente solicitando spoof (cgiirc padrão para cgiirc).
* hostname: Nome do host do usuário.
* ip: IP, seja em notação IPv4 pontilhada quad (eg 192.0.0.2) ou notação IPv6 (por exemplo 1234: 5678: 9abc :: def). Os endereços IPv4 em IPv6 (ex .: :: ffff: 192.0.0.2) não devem ser enviados.

A senha deve ser acordada anteriormente com o servidor IRC ao qual o cliente está se conectando. Normalmente isso será definido no arquivo de configuração.

# Expectativas

## Expectativas do cliente

* Execute qualquer resolução de proxy (TrustedProxies no CGI: IRC)
* Verificar o DNS inverso e encaminhar combinação de DNS
* Verifique o IP contra os controles de acesso adequados (ipaccess, dnsbl em CGI: IRC)

## Expectativas do servidor

* Verifique o host e a senha de conexão
* Defina o host eo endereço IP para o endereço fornecido
* Aplicar proibições (de servidor e de canal) em relação aos endereços fornecidos no comando WEBIRC


## Considerações de segurança

O servidor deve limitar os hosts dos quais um comando WEBIRC é aceito. Qualquer pessoa com uma senha válida e host para conectar-se pode spoof qualquer nome de host que eles desejam, isso é principalmente por design. (É possível que o IRCD verifique se o IP corresponde ao hostname para parar a falsificação de qualquer host). Portanto, é possível usar isso para ignorar a maioria dos tipos de ban. Recomenda-se que o IRCD forneça um método para localizar o host original e mostre que o WEBIRC está em uso (para os operadores de IRC pelo menos) para fornecer uma maneira de lidar com o abuso.



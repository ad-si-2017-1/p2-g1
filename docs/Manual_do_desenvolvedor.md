# Introdução
    
    O projeto 2 sprint 2, iremos inciar a documentação do desenvolvedor e do usuário para dar continuidade nas demais sprints.

## Obejtivo

* Guiar colaboradores no desenvolvimento do software;
* Equipe e resonsabilidade dos membros;
* Conceitos básicos e arquiteturas;
* licença de software;
* Referências.

## Membros

* Erick Vinicius - (Documentador) - Criar o manual do usuário e desenvolvedor.
* Anderson - (Líder - Desenvolvedor) - Criação da aplicação e suas funcionalidades.
* Silvio - (Desenvolvedor) - Criação da aplicação e suas funcionalidades.

## WebSockets

Em muitos dos sistemas de informação atuais a pronta resposta a ações do usuário tem se tornado um requisito quase que obrigatório. Dependendo do tipo de aplicação, não é
recomendado que o usuário espere muito tempo até que se tenha um feedback. 

    Esse problema torna-se ainda mais complexo em aplicações Web, dado o tipo de arquitetura das aplicações. Assim, é indispensável nos dias de hoje que o atraso de comunicação 
entre servidor e o cliente seja quase nulo e o limite de atraso sempre seja reduzido. Em relação aos atrasos de comunicação e de forma a obter latências de conexões quase nulas
ou em tempo real entre clientes e servidores é necessário que utilizemos mecanismos os quais oferecem mais recursos do que o serviço oferecido pelo protocolo HTTP.

    Os WebSockets surgiram para prover esse tipo de serviço com retardo reduzido, o qual o HTTP não oferece apoio atualmente. O WebSocket é um padrão ainda em desenvolvimento que 
está sob os cuidados da IETF (Internet Enginnering Task Force) e uma API padrão para implementação do protocolo está em processo de formalização pela W3C (World Wide Web 
Consortium) para que os navegadores ofereçam apoio ao protocolo. O suporte dos navegadores ao protocolo já se encontra em desenvolvimento e disponibilização ao usuário 
final, entretanto utilizar as versões mais recentes dos navegadores será indispensável para quem quiser desenvolver sistemas de informação utilizando essa arquitetura e usufruir 
do serviço que está em constante aprimoramento.

    Por implementar um canal de transmissão bidirecional e por ter um cabeçalho menor, o WebSocket supera o HTTP em relação ao número de requisições feitas e no tamanho das 
mensagens. 

    Em relação ao requisito de segurança, podemos utilizar o WebSocket secure (WSS) como uma alternativa de meio de transmissão seguro em relação ao WebSocket convencional.
    
## Arquitetura WebSocket

![Arquitetura WebSocket](https://imageshack.com/i/pm4d3WjPj)
    
    
Os WebSockets HTML5 suportam riscos de rede, como proxies e firewalls, tornando o streaming possível em qualquer conexão e com a capacidade de suportar comunicações de upstream e downstream em uma única conexão, os aplicativos baseados em WebSockets HTML5 colocam menos carga nos servidores, permitindo que máquinas existentes suportem Mais conexões simultâneas. A figura a seguir mostra uma arquitetura baseada em WebSocket básica na qual os navegadores usam uma conexão WebSocket para comunicação direta com hosts remotos.

Um WebSocket detecta a presença de um servidor proxy e configura automaticamente um túnel para passar pelo proxy. O túnel é estabelecido através da emissão de uma instrução HTTP CONNECT para o servidor proxy, que solicita que o servidor proxy abra uma conexão TCP / IP para um host e porta específicos. Uma vez que o túnel é configurado, a comunicação pode fluir sem impedimentos através do proxy. Como o HTTP / S funciona de forma semelhante, o WebSockets seguro através do SSL pode alavancar a mesma técnica HTTP CONNECT. 


## Socket.io

O Socket.IO oferece uma API de JavaScript simples, baseada em eventos que te permite comunicar entre o servidor e o cliente sem esforço e em tempo real. Seu mecanismo padrão é o WebSockets, mas se não for implementado no navegador do usuário, ele recorrerá à fallbacks, como Flash e AJAX. Isso o torna disponível para um grande número de navegadores, como você pode ver aqui.

A biblioteca Socket.IO convencional é escrita em JavaScript tanto para front end, quanto para back-end, por isso foi projetada para rodar em um servidor Node.js.
    
## Proxy

Em redes de computadores , um servidor proxy é um servidor (um sistema de computador ou um aplicativo) que atua como um intermediário para solicitações de clientes que buscam recursos de outros servidores. Um cliente se conecta ao servidor proxy, solicitando algum serviço, como um arquivo, conexão, página da Web ou outro recurso disponível de um servidor diferente e o servidor proxy avalia o pedido como uma forma de simplificar e controlar sua complexidade. Proxies foram inventados para adicionar estrutura e encapsulamento a sistemas distribuídos. Hoje, a maioria dos proxies são proxies web , facilitando o acesso ao conteúdo na WWW (World Wide Web.)


![Arquitetura Proxy](https://imageshack.com/i/pmzV1IAxg)

Como podemos ver na imagem acima, o cliente proxy fica como intermediário, assim toda requisição feita pelo cliente web (HTTP), o proxy irá receber a mensagem e irá intermediar a mensagem para o servidor de aplicação, no nosso caso o servidor IRC que se comunica atraves de websocket.

## Nodejs

Node.js é um interpretador de código JavaScript que funciona do lado do servidor. Seu objetivo é ajudar programadores na criação de aplicações de alta escalabilidade (como um servidor web), com códigos capazes de manipular dezenas de milhares de conexões simultâneas, numa única máquina física. O Node.js é baseado no interpretador V8 JavaScript Engine (interpretador de JavaScript open source implementado pelo Google em C++ e utilizado pelo Chrome). 

## Express

O Express é um framework para aplicativo da web do **Node.js** mínimo e flexível que fornece um conjunto robusto de recursos para aplicativos web e móvel, com uma varíedade de métodos utilitários HTTP e middleware a seu dispor, criar uma API robusta é rápido e fácil. O Express fornece uma camada fina de recursos fundamentais para aplicativos da web, sem obscurecer os recursos do Node.js que você conhece e ama.
Entregando arquivos estáticos no Express

Para entregar arquivos estáticos como imagens, arquivos CSS, e arquivos JavaScript, use a função de middleware express.static integrada no Express.
Passe o nome do diretório que contém os ativos estáticos para a função de middleware express.static para iniciar a entregar os arquivos diretamente. Por exemplo, use o código a seguir para entregar imagens, arquivos CSS, e arquivos JavaScript em um diretório chamado public:

```javascript
app.use(express.static('public'));
```
Agora, é possível carregar os arquivos que estão no diretório public:

```javascript
http://localhost:3000/images/kitten.jpg
http://localhost:3000/css/style.css
http://localhost:3000/js/app.js
http://localhost:3000/images/bg.png
http://localhost:3000/hello.html
```

O Express consulta os arquivos em relação ao diretório estático, para que o nome do diretório estático não faça parte da URL.
Para usar vários diretórios de ativos estáticos, chame a função de middleware express.static várias vezes:


```javascript
app.use(express.static('public'));
app.use(express.static('files'));
```

O Express consulta os arquivos na ordem em que você configurar os diretórios estáticos com a função de middleware express.static.

Para criar um prefixo de caminho virtual (onde o caminho não existe realmente no sistema de arquivos) para arquivos que são entregues pela função express.static, especifique um caminho de montagem para o diretório estático, como mostrado abaixo:


```javascript
app.use('/static', express.static('public'));
```

Agora, é possível carregar os arquivos que estão no diretório public a partir do prefixo do caminho /static.


```javascript
http://localhost:3000/static/images/kitten.jpg
http://localhost:3000/static/css/style.css
http://localhost:3000/static/js/app.js
http://localhost:3000/static/images/bg.png
http://localhost:3000/static/hello.html
```

Entretanto, o caminho fornecido para a função express.static é relativa ao diretório a partir do qual você inicia o seu node de processo. Se você executar o aplicativo express a partir de outro diretório, é mais seguro utilizar o caminho absoluto do diretório para o qual deseja entregar.

```javascript
app.use('/static', express.static(__dirname + '/public'));
```

##Utilização de Cookies
**O que é um cookie?**<br>
Os cookies são pequenos pedaços de dados enviados de um site e são armazenados no navegador do usuário enquanto o usuário está navegando nesse site. Toda vez que o usuário carrega o site de volta, o navegador envia os dados armazenados de volta para o site ou servidor, para distinguir a atividade anterior do usuário.

A primeira coisa que você estaria fazendo é instalar _cookie-parser_ middleware através npm em **node_modules** diretório onde pode ser encontrado na sua pasta de aplicativos.<br>E para instalá-lo:
Abra seu terminal,<br>
Navegue até a pasta da sua aplicação,<br>
```javascritpt
$ npm install cookie-parser
```
###Usando o Cookie-Parser
Importar ** cookie-parser ** para o seu aplicativo
```javascript
var express = require('express');
var cookieParser = require('cookie-parser');
var app = express();
app.use(cookieParser());
```
###Sintaxe
O analisador de cookies analisa o cabeçalho de cookie e preenche req.cookies com um objeto codificado pelos nomes de cookie. Para definir um novo cookie, defina uma nova rota no aplicativo expresso como:
```javascript
app.get('/cookie',function(req, res){
     res.cookie(cookie_name , 'cookie_value').send('Cookie is set');
});
```
Para verificar se o cookie foi definido ou não, vá para o console do navegador e escreva **document.cookie**.
O navegador envia de volta esse cookie para o servidor, sempre que solicita esse site. E para obter um cookie que um navegador pode estar enviando para o servidor, anexando-o ao cabeçalho da solicitação, podemos escrever o seguinte código:
```javascript
app.get('/', function(req, res) {
  console.log("Cookies :  ", req.cookies);
});
```
**Como definir o tempo de validade do cookie?**
Tempo de expiração de cookies pode ser definido facilmente por:
```javascript
res.cookie(name , 'value', {expire : new Date() + 9999});
```
As opções de adição de cookies podem ser definidas, passando um objeto como argumento que carrega configurações adicionais para cookies. Assim, para definir o tempo de expiração para cookies, um objeto com expirar propriedade pode ser enviado que mantém o tempo de expiração em milissegundos.

Uma abordagem alternativa para definir a idade de expiração do cookie é usar a propriedade magAge opcional .

res.cookie(name, 'value', {maxAge : 9999});

**Como excluir Cookie existente?**
Os cookies existentes podem ser apagados muito facilmente usando o método **clearCookie** , que aceita o nome do cookie que você deseja excluir.
```javascript
app.get('/clearcookie', function(req,res){
     clearCookie('cookie_name');
     res.send('Cookie deleted');
});
```
Agora, mais uma vez você pode ir e verificar no console do navegador que o cookie específico foi excluído.

## NPM

O NPM - Node Package Manager, é muito útil para gerenciar módulos. Sua instalação já vem quando se instala o Node.js e utilizá-lo é muito simples.
Obs.: Por padrão cada módulo é instalado em modo local, ou seja, é apenas inserido dentro do diretório atual do projeto. Caso queria instalar módulos em módulo global apenas inclua o parâmetro -g em cada comando.
Por exemplo: 

```javascript
npm install -g nome_do_módulo
```

A seguir, seguem alguns comandos básicos e essenciais para gerenciamento de módulos em um projeto node:

**npm install nome_do_módulo**: instala um módulo no projeto.<br>
**npm install nome_do_módulo --save**: instala o módulo e adiciona-o na lista de dependências do package.json do projeto.<br>
**npm list**: lista todos os módulos existentes no projeto.<br>
**npm list -g**: lista todos os módulos globais.<br>
**npm remove nome_do_módulo**: desinstala um módulo do projeto.<br>
**npm update nome_do_módulo**: atualiza a versão do módulo.<br>
**npm -v**: exibe a versão atual do npm.<br>
**npm adduser nome_do_usuário**: cria um usuário no site https://npmjs.org para publicar seu módulo na internet.<br>
**npm whoami**: exibe detalhes do seu perfil público do npm (é necessário criar um usuário com o comando anterior).<br>
**npm publish**: publica o seu módulo, é necessário ter uma conta ativa no site oficial: <https://npmjs.org> , onde podemos ter maiores detalhes, ou acessar o seu código-fonte: <https://github.com/isaacs/npm>.

## NPM INIT 

Para quem não conhece esse comando, o **npm init** apresenta um simples questionário para descrever informações básica de um módulo Node.js, pelo qual seu resultado final é gerar o arquivo **package.json**.

Porém, é possível customizar o resultado desse comando, incrementando novas informações para agilizar na geração do **package.json** a cada novo projeto. Para fazer essa simples customização, basta criar na pasta raíz de usuário do sistema o arquivo: **~/.npm-init.js**, nele você pode injetar valores pré-definidos para alguns campos chaves na geração de um **package.json**, veja esse exemplo:

 > exports.name = prompt('Name', basename);<br>
 > exports.description = prompt('Description', '');<br>
 > exports.version = prompt('Version', config.get('init-version'));<br>
 > exports.main = config.get('main-file');<br>
 > exports.license = config.get('init-license');<br>
 > exports.author = config.get('init-author-name') + '<'+config.get('init-author-email')+'>';<br>
 > exports.scripts = {<br>
 >  start: 'node ' + config.get('main-file'),<br>
 >  prestart: 'npm install'<br>
 };<br>
 > exports.engines = {<br>
 >  node: '>= 4.4.7',<br>
 >  npm: '>= 2.15.8'<br>
 > };<br>
 
Cada **exports.chave** é referente a um atributo a ser incluído no **package.json**. Outro detalhe mais legal, é que é possível deixar algumas das principais perguntas já respondidas com valores default, para isso basta editar o arquivo **~/.npmrc**, incluindo nele algums campos, como por exemplo:

> init-license='MIT'<br>
> init-author-name='Anderson Martins Dias'<br>
> init-author-email='andersonsystemas@gmail.com'<br>
> init-version='1.0.0'<br>
> main-file='app.js'<br>

Com isso, praticamente você nem terá mais que responder as principais perguntas, pois elas já serão pré-definidas pelos valores existentes no **~/.npmrc**, tanto é que elas são acessadas magicamente pela função **config.get('nome-de-uma-key')**.

Agora se você criar um novo projeto e rodar o comando **npm init** você terá um resultado semelhante a este:

![Package.json](http://i66.tinypic.com/16llkqx.png)

## Funções de CallBack Encadeadas

Vimos o quanto é vantajoso e performático trabalhar de forma assíncrona, porém em certos momentos, inevitavelmente implementaremos diversas funções assíncronas, que serão encadeadas uma na outra através das suas funções callback. No código a seguir apresentarei um exemplo desse caso, crie um arquivo chamado **callback_hell.js**, implemente e execute o código abaixo:

 ```javascript
 var fs = require('fs');
 fs.readdir(__dirname, function(erro, contents) {
   if (erro) { throw erro; }
   contents.forEach(function(file) {
     var path = './' + file;
     fs.stat(path, function(erro, stat) {
       if (erro) { throw erro; }
       if (stat.isFile()) {
         console.log('%s %d bytes',file,stat.size);
       }
     });
   });
 });
```
 
Observe a quantidade de callbacks encadeados existem em neste código, detalhe, ele apenas faz uma simples leitura dos arquivos de seu diretório e imprime na tela seu nome e tamanho em bytes. Um pequena tarefa como essa deveria ter menos encadeamentos, concorda? Agora imagine como seria a organização disso para realizar tarefas mas complexas? Praticamente seria um caos o seu código e totalmente difícil de fazer manutenções.

Por ser assíncrono você perde o controle do que esta executando em troca de ganhos com performance, porém, um detalhe importante sobre assincronismo é que na maioria dos casos os callbacks bem elaborados possuem como parâmetro uma variável de erro. Verique nas documentações sobre sua existência e sempre faça o tratamento deles na execução do seu callback: if (erro) { throw erro; }, isso vai impedir a continuação da execução aleatória quando for identificado um erro.

Uma boa prática de código Javascript é criar funções que expressem seu objetivo e de forma isoladas, salvando em variável e passando-as como callback, ao invés de criar funções anônimas, por exemplo, crie um arquivo chamado callback_heaven.js com o código abaixo:
```javascript
 var fs = require('fs');
 var lerDiretorio = function() {
   fs.readdir(__dirname, function(erro, dir) {
     tratar(erro);
     dir.forEach(function(file) {
       ler(file);
     });
   });
 };
 var ler = function(file) {
   var path = './' + file;
   fs.stat(path, function(erro, stat) {
     tratar(erro);
     if (stat.isFile()) {
       console.log('%s %d bytes',file,stat.size);
     }
   });
 };
 var tratar = function(erro) {
   if (erro) { throw erro; }
 };
 lerDiretorio();
 ```
Veja o quanto melhorou a legibilidade do seu código. Dessa forma foi possível reaproveitar a função tratar(erro) em dois locais do código, deixamos mais semântico e legível o nome das funções e diminuímos o encadeamento de callback das funções assíncronas. A boa prática é ter o bom senso de manter no máximo até dois encadeamentos de callbacks, ao passar disso significa que esta na hora de criar uma nova função fora externa para ser chamada no último callback, ao invés de continuar criando um callback hell em seu código.



## Teste-IRC

Abaixo serão apresentados alguns dos eventos implementados
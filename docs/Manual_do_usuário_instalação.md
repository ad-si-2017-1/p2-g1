## Introdução

    No guia iremos mostrar meios de instalar os frameworks utilizados para esse projeto.
## Objetivo

* Guiar o usuário na utilização do software;
* Instalação;
* Configuração;
* Teste das funcionalidades;
* Lista de bugs.

## Instalação


### Nodejs

Instalando o node.js através do nvm, você pode utilizar o script de instalação usando Curl.

    Curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | Bash
    
Ou Wget:

    Wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | Bash
    
O script clona o repositório nvm para ~/.nvm e acrescenta a linha da fonte ao seu perfil ( ~/.bash_profile, ~/.zshrc, ~/.profileou ~/.bashrc).

    Exportar NVM_DIR = " $ HOME /.nvm " 
    [ -s  " $ NVM_DIR /nvm.sh " ] &&  .  " $ NVM_DIR /nvm.sh "  # Isso carrega nvm

Você pode personalizar a instalação de origem, diretório, perfil e versão usando os NVM_SOURCE, NVM_DIR, PROFILE, e NODE_VERSIONvariáveis. Ex .: curl ... | NVM_DIR=/usr/local/nvm bashpara uma instalação global.

### Verificar instalação

Para verificar se o nvm foi instalado, faça:

    Command -v nvm
    
## Uso

Para baixar, compilar e instalar a versão mais recente do node, faça o seguinte:

    Nvm install node
    
E então em qualquer novo shell basta usar a versão instalada:

    Nvm use node
    
Ou você pode apenas executá-lo:

    nvm run node --version
    
Ou, você pode executar qualquer comando arbitrário em um subshell com a versão desejada do node:

    Nvm exec 4.2 node --version # sempre escolher a versão mais atualizada
    
Você também pode obter o caminho para o executável para onde ele foi instalado:

    Nvm which 5.0 # sempre escolher a versão mais atualizada
    
No lugar de um ponteiro de versão como "0,10" ou "5.0" ou "4.2.1", você pode usar os seguintes aliases padrão especiais com nvm install, nvm use, nvm run, nvm exec, nvm which, etc:

## Instalando nodejs compilando das fontes do site oficial.

Também é possivel fazer a instalação do nodejs compilando as fontes do site oficial.

Atualize o sistema e instale as dependências via apt-get:

    apt-get update && apt-get install git-core curl build-essential openssl libssl-dev make g++
    
Crie um diretório chamado nodejs e entre nele cd nodejs, você pode fazer isso em um único passo:

    mkdir ~/nodejs && cd $_
    
Baixo os fontes com o utilitário wget:

    wget -N http://nodejs.org/dist/node-latest.tar.gz
    
Descompacte o arquivo:

    tar xzvf node-latest.tar.gz
    
Entre na pasta node-v*:

    cd `ls -rd node-v*`
    
Compile:

    ./configure
    make
    make install
    
Para testar execute:

    node -v
    

### Instalando Socket.io

    npm install socket.io --save

### Instalando o Express

Assumindo que já tenha instalado o Node.js, crie um diretório para conter o seu aplicativo, e torne-o seu diretório ativo.

    mkdir myapp
    cd myapp
    
Use o comando npm init para criar um arquivo package.json para o seu aplicativo.

    npm init
    
Este comando solicita por várias coisas, como o nome e versão do seu aplicativo. Por enquanto, é possível simplesmente pressionar RETURN para aceitar os padrões para a maioria deles, com as seguintes exceções:

    entry point: (index.js)
    
Insira app.js, ou qualquer nome que deseje para o arquivo principal. Se desejar que seja index.js, pressione RETURN para aceitar o nome de arquivo padrão sugerido.

Agora instale o Express no diretório app e salve-o na lista de dependências. Por exemplo.

    npm install express --save

### Instalando IRC

    npm install node-irc


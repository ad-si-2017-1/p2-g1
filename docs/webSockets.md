#WebSockets

>O **WebSocket** era parte da especificação do [HTML5](https://www.html5rocks.com/pt/tutorials/websockets/basics/) que descreve a comunicação bidirecional (envio e recebimento de mensagens) entre canais através de TCP/IP.
>Deixou de fazer parte do HTML5 e ganhou vida própria, sendo o protocolo padronizado pela IETF como [RFC 6455](http://tools.ietf.org/html/rfc6455) e a API padronizada pela W3C.
>Não que a ideia do WebSocket seja totalmente nova – pois sempre demos o nosso jeitinho através do Flash, do Long Pulling e do Ajax – que também são eficientes e possuem melhor compatibilidade com o nosso atual ambiente (visto que o WebSocket é muito novo), porém não sem cobrar o seu preço: o que antes era caro e despadronizado, agora se torna barato, uniforme e eficiente.
>Há uma implementação do **WebSocket** sensacional para Node.js e Javascript, chamada Socket.IO: ele não só incorpora tudo o que é necessário para nos comunicarmos através de **WebSocket**, mas também oferece alternativas implícitas caso os ambientes utilizados não oferecem suporte à WebSocket, por serem antigos.
>E melhor: tudo isto através de uma interface extremamente simplificada e fácil de entender. Isto coloca a API como uma das mais populares quando falamos em termos de WebSocket. Atrelada aos principais problemas que o Node.js resolve, este duplo combo é quase que imbatível.
>Portanto, coders de modo geral, deixem sua camisa de lado e seja bem-vindo ao mundo da programação poliglota, onde utilizamos o que há de melhor disponível no mercado para desenharmos uma solução – como oposição a ficar refém de um único vendor, hasteando a bandeira de que ali reside toda a verdade.
>Se você é um destes, eis aqui uma boa porta de entrada para o universo do Node.js – na trilha de desenvolvimento de software, esta já considerada uma das melhores tecnologias dos últimos anos e grandes empresas estão adotando-a, como a Microsoft e o seu Visual Studio Online, que foi criado com Node.js, e como a PayPal, que abandonou o Java para utilizar Node.js em seu principal produto.
>Se você não sabe nada de Node.js e está com preguiça de pesquisar, vou resumir: os caras deram um jeito de compilar Javascript do lado do servidor – ponto-final. Aquilo que você fazia no client, agora consegue fazer também no server (exatamente da mesma forma).
>Neste caso, a API (que no Node.js é chamada de módulo) Socket.IO é a mesma no *client* e no *server*.
>O Socket.IO abstrai e utiliza o WebSocket como o seu principal mecanismo. E se não for compatível com o meu ambiente? Então o próprio Socket.IO se encarrega de providenciar outros mecanismos de transporte, como fallback:

- Adobe® Flash® Socket
- AJAX long polling
- AJAX multipart streaming
- Forever Iframe
- JSONP Polling

>Não precisamos nos preocupar, por que tudo é transparente! Dá para entender por que o módulo é tão popular – pois garante compatibilidade entre diferentes clientes e servidores, sejam através de dispositivos móveis ou desktops, independente de versão de browser e sistema operacional.

**E como o WebSocket funciona?**

>Basicamente ouvimos e transmitimos mensagens. Neste caso, você tem basicamente dois métodos que vão ditar todo o restante: on e emit. Simples assim. No on você deixa o servidor ou o client aguardando uma mensagem chegar e no emit você transmite esta mensagem.
>É isto aí amigo: para você que tem tanto trauma de Sockets, só precisa entender estes dois métodos! Se entendeu, você já concluiu 85% do curso de Socket.IO!
>Vamos a um fluxo básico para você frisar melhor (se refere ao registro em um chat).

![Alt text](https://evenancio.files.wordpress.com/2013/12/websocket-chat.png)

Bom, é isto! Você simplesmente envia (**emit**) uma mensagem para alguém que esteja esperando (**on**) por ela. Quando ele recebe, ele faz algo com isto e pode, inclusive, responder com uma mensagem (**emit**) para o cliente, caso ele esteja aguardando (**on**) por um retorno.





[Artigo WebSockets - EVANDRO VENANCIO](https://evenancio.wordpress.com/2013/12/21/websocket-com-node-js-parte-1-introducao-ao-socket-io/)